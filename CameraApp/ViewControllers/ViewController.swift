//
//  ViewController.swift
//  CameraApp
//
//  Created by macexpert on 29/09/20.
//  Copyright © 2020 macexpert. All rights reserved.
//

import UIKit
import FacebookLogin
import FacebookCore
import GoogleSignIn


@available(iOS 13.0, *)

class ViewController: UIViewController,GIDSignInDelegate {
    
    @IBOutlet var email_BG_View: UIView!
    @IBOutlet var Pwd_BG_View: UIView!

// UITextFields
    @IBOutlet var txt_Email: UITextField!
    @IBOutlet var txt_Pwd: UITextField!

// UIButtons
    @IBOutlet var btn_Login: UIButton!
    
    var instagramApi = InstagramApi.shared
    var testUserData = InstagramTestUser(access_token: "", user_id: 0)
    var instagramUser: InstagramUser?
    var signedIn = false
    
    @IBOutlet var backgroundImageView: UIImageView!
    
    @IBAction func instagram_Tapped(_ sender: UIButton) {
         if self.testUserData.user_id == 0 {
              presentWebViewController()
          } else {
              self.instagramApi.getInstagramUser(testUserData: self.testUserData) { [weak self] (user) in
                  self?.instagramUser = user
                  self?.signedIn = true
                  DispatchQueue.main.async {
                      self?.presentAlert()
                  }
              }
          }
     }
    
    @IBAction func fetchImageToBackground(_ sender: UIButton) {
        if self.instagramUser != nil {
            self.instagramApi.getMedia(testUserData: self.testUserData) { (media) in
                if media.media_type != MediaType.VIDEO {
                    let media_url = media.media_url
                    self.instagramApi.fetchImage(urlString: media_url, completion: { (fetchedImage) in
                        if let imageData = fetchedImage {
                            DispatchQueue.main.async {
                                self.backgroundImageView.image = UIImage(data: imageData)
                            }
                        } else {
                            print("Didn't fetched the data")
                        }
                        
                    })
                    print(media_url)
                } else {
                    print("Fetched media is a video")
                }
            }
        } else {
            print("Not signed in")
        }
    }
    func presentAlert() {
        let alert = UIAlertController(title: "Signed In:", message: "with account: @\(self.instagramUser!.username)", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true)
        
        let instagramID = "with account ID: \(self.instagramUser!.id)"
        print("instagramID===  \(instagramID)")
    }
    
    func presentWebViewController() {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let webVC = storyBoard.instantiateViewController(withIdentifier: "webView") as! WebViewController
        webVC.instagramApi = self.instagramApi
        webVC.mainVC = self
        self.present(webVC, animated:true)
    }
    
    override func viewDidLoad() {

        super.viewDidLoad()
        
      // Google
        GIDSignIn.sharedInstance()?.delegate = self
        
    // Static Data
        txt_Email.text = "test@gmail.com"
        txt_Pwd.text = "12345"

        self.navigationController?.isNavigationBarHidden = true
        self.email_BG_View.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        self.email_BG_View.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        self.email_BG_View.layer.shadowOpacity = 1.0
        self.email_BG_View.layer.shadowRadius = 0.0
        self.email_BG_View.layer.masksToBounds = false
        self.email_BG_View.layer.cornerRadius = 5.0

        self.Pwd_BG_View.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        self.Pwd_BG_View.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        self.Pwd_BG_View.layer.shadowOpacity = 1.0
        self.Pwd_BG_View.layer.shadowRadius = 0.0
        self.Pwd_BG_View.layer.masksToBounds = false
        self.Pwd_BG_View.layer.cornerRadius = 5.0
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let checkMode =  "\(UserDefaults.standard.value(forKey: "MODETYPE") ?? "")"
         if checkMode == "DARK"{
             if #available(iOS 13.0, *) {
                       overrideUserInterfaceStyle = .dark
                     } else {
                         // Fallback on earlier versions
                     }
         } else {
             if #available(iOS 13.0, *) {
                 overrideUserInterfaceStyle = .light
             } else {
                 // Fallback on earlier versions
             }
         }
    }
    @IBAction func signup_Tapped(_ sender: UIButton) {
       let vc = storyboard?.instantiateViewController(withIdentifier: "SignUpVC") as! SignUpVC
       self.navigationController?.pushViewController(vc, animated: true)
    }

    @IBAction func login_Tapped(_ sender: UIButton) {
        if (txt_Email.text == "")
         {
            let message = UIAlertView(title: "ConnectedSolutions", message: "Please enter Email", delegate: nil, cancelButtonTitle: "OK")
            message.show()
         }
        else if !(txt_Email.text?.isvaliddemail())!{
           let message = UIAlertView(title: "ConnectedSolutions", message: "Please enter Valid Email", delegate: nil, cancelButtonTitle: "OK")
           message.show()
         }
         else if (txt_Pwd.text == "")
         {
            let message = UIAlertView(title: "ConnectedSolutions", message: "Please enter your password", delegate: nil, cancelButtonTitle: "OK")
             message.show()
         }
         else if (txt_Email.text != ""  && txt_Pwd.text != "")
         {
            if let image = UIImage(named: "button-login (active)") {
               btn_Login.setImage(image, for: .normal)
            }
            self.performSegue(withIdentifier: "mySegueIdentifier", sender: nil)
         }
         else {
    //   loginApi()
      }
 }
    @IBAction func facebook_Tapped(_ sender: UIButton) {
        facebookManager()
    }
    func fetchUserProfile()
    {
        let graphRequest : GraphRequest = GraphRequest(graphPath: "me", parameters: ["fields":"id, email, name, picture.width(480).height(480)"])

        graphRequest.start(completionHandler: { (connection, result, error) -> Void in
            
            if ((error) != nil)
            {

                print("Error took place: \(String(describing: error))")
            }
            else
            {
                print("Print entire fetched result: \(String(describing: result!))")
            //  SocialId = AccessToken.current!.userID
                
                let alertController = UIAlertController(
                    title: "Login Success",
                    message: "Facebook Login Succeeded",
                    preferredStyle: UIAlertController.Style.alert
                )
                alertController.addAction(UIAlertAction.init(title: "ok", style: .default, handler: { (UIAlertaction) in
                    print("om")
                    print(AccessToken.current!.userID)
                  //  self.Signin()
                }))
                self.present(alertController, animated: true, completion: nil)
            }
        })
}
        @objc func facebookManager() {
               let loginManager = LoginManager()
               loginManager.logIn(
                permissions: [.publicProfile, .userFriends, .email],
                   viewController: self
               ) { result in
                   self.loginManagerDidComplete(result)
               }
           }
        
        func loginManagerDidComplete(_ result: LoginResult) {
                  let alertController: UIAlertController
                  switch result {
                  case .cancelled:
                      alertController = UIAlertController(
                          title: "Login Cancelled",
                          message: "User cancelled login.",
                          preferredStyle: UIAlertController.Style.alert
                      )
        
                  case .failed(let error):
                      alertController = UIAlertController(
                          title: "Login Fail",
                          message: "Login failed with error \(error)",
                          preferredStyle: UIAlertController.Style.alert
                      )
                    
        
                  case .success(let grantedPermissions ):
                      alertController = UIAlertController(
                          title: "Login Success",
                          message: "Login succeeded",
                          preferredStyle: UIAlertController.Style.alert
                          
                      )
                   
                      //fetchUserProfile()
                    let SocialId = "\(AccessToken.current!.userID)"
                    print(SocialId)
            }
                 // alertController.addAction(UIAlertAction.init(title: "ok", style: .default, handler: nil))
            alertController.addAction(UIAlertAction.init(title: "ok", style: .default, handler: { (signin) in
               // self.Signin()
            }))
                  self.present(alertController, animated: true, completion: nil)
    }

    @IBAction func google_Tapped(_ sender: UIButton) {
        GIDSignIn.sharedInstance()?.presentingViewController = self
        GIDSignIn.sharedInstance()?.signIn()
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if error == nil {
            print(user.userID!)
           
            let SocialId = user.userID
            print("SocialId==== \(String(describing: SocialId))")

            print(user.profile!)
            print(user.profile.givenName!)
          //  firstname = user.profile.givenName!
            print(user.profile.familyName!)
          //  lastname = user.profile.familyName!
            print(user.profile.email!)
         //   Signin()
            print(user.profile.imageURL(withDimension: 480)!)
            
            
            let alertController = UIAlertController(
                title: "Login Success",
                message: "Google Login Succeeded",
                preferredStyle: UIAlertController.Style.alert
            )
            alertController.addAction(UIAlertAction.init(title: "ok", style: .default, handler: { (UIAlertaction) in
                print(AccessToken.current!.userID)
              //  self.Signin()
            }))
            self.present(alertController, animated: true, completion: nil)
        }
}

 
}

// For check Email Vaid or not
extension String{
    func isvaliddemail() -> Bool {
        let emailFormat = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
        return emailPredicate.evaluate(with: self)
    }
}
