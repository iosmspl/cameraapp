//
//  viewAllVC.swift
//  CameraApp
//
//  Created by macexpert on 03/10/20.
//  Copyright © 2020 macexpert. All rights reserved.
//

import UIKit

class viewAllVC: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate {
    
    @IBOutlet var ViewAllcollectionView: UICollectionView!
    
    @IBOutlet var profile_BGVIew: UIView!
    @IBOutlet var btn_profile: UIButton!
    
    let screenWidth = UIScreen.main.bounds.size.width
    let screenHeight = UIScreen.main.bounds.size.height
    let reuseIdentifier = "cell" // also enter this string as the cell identifier in the storyboard
    
    var items = ["1", "2", "3", "4", "5"]
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        
        // For set the profile image corners rounded
     //   self.profile_BGVIew.layer.cornerRadius = self.profile_BGVIew.frame.height / 2
        
        
      //  profile_BGVIew.layer.cornerRadius = self.profile_BGVIew.frame.height / 2
      //  profile_BGVIew.clipsToBounds  =  true
        
        btn_profile.layer.cornerRadius = self.btn_profile.frame.height / 2
        btn_profile.clipsToBounds = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let checkMode =  "\(UserDefaults.standard.value(forKey: "MODETYPE") ?? "")"
         if checkMode == "DARK"{
             if #available(iOS 13.0, *) {
                       overrideUserInterfaceStyle = .dark
                     } else {
                         // Fallback on earlier versions
                     }
         } else {
             if #available(iOS 13.0, *) {
                 overrideUserInterfaceStyle = .light
             } else {
                 // Fallback on earlier versions
             }
         }
    }

    // MARK: - UICollectionViewDataSource protocol
        // tell the collection view how many cells to make
        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return self.items.count
        }

    // make a cell for each cell index path
        func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            
            // get a reference to our storyboard cell
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath as IndexPath) as! viewAllCell
            
            // Use the outlet in our custom class to get a reference to the UILabel in the cell
         //   cell.myLabel.text = self.items[indexPath.row] // The row value is the same as the index of the desired text within the array.
        //    cell.backgroundColor = UIColor.cyan // make cell more visible in our example project
            
            return cell
        }
    
    // MARK: - UICollectionViewDelegate protocol
        func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            // handle tap events
            print("You selected cell #\(indexPath.item)!")
   }
    @IBAction func profile_Tapped(sender: AnyObject) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "profileVC") as! profileVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

class viewAllCell: UICollectionViewCell {
    @IBOutlet weak var myLabel: UILabel!
}

extension viewAllVC: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: screenWidth / 2, height: 177.0)
    }
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    func collectionView(_ collectionView: UICollectionView, layout
        collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
}
//}

extension UIView {
   func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
}
