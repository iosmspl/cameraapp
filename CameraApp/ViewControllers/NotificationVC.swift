//
//  NotificationVC.swift
//  CameraApp
//
//  Created by macexpert on 03/10/20.
//  Copyright © 2020 macexpert. All rights reserved.
//

import UIKit
import IFGZoomCollectionViewLayout

let collection = IFGZoomCollectionViewLayout()

class NotificationVC: UIViewController,UITableViewDataSource,UITableViewDelegate,UICollectionViewDataSource,UICollectionViewDelegate {

    @IBOutlet var tblView_Notification: UITableView!
    @IBOutlet var datescollectionView: UICollectionView!

    let aryNotificationList = ["House Entrance", "Kitchen", "House Entrance", "Kitchen"]
    
    var items = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30"]

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.isNavigationBarHidden = true
        self.datescollectionView.collectionViewLayout = collection
        
        collection.cellZoomInSize = CGSize(width: 50, height: 50)
        collection.cellZoomOutSize = CGSize(width: 50, height: 50)
    }
    override func viewWillAppear(_ animated: Bool) {
        let checkMode =  "\(UserDefaults.standard.value(forKey: "MODETYPE") ?? "")"
         if checkMode == "DARK"{
             if #available(iOS 13.0, *) {
                       overrideUserInterfaceStyle = .dark
                     } else {
                         // Fallback on earlier versions
                     }
         } else {
             if #available(iOS 13.0, *) {
                 overrideUserInterfaceStyle = .light
             } else {
                 // Fallback on earlier versions
             }
         }
    }
    // MARK: - UICollectionViewDataSource protocol
          // tell the collection view how many cells to make
      func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
          return self.items.count
      }

    // make a cell for each cell index path
      func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
          
          // get a reference to our storyboard cell
          let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath as IndexPath) as! datesCell
          cell.lbl_Date.text = items[indexPath.row]
        
        if let centerindexpath = datescollectionView.indexPathForItem(at: datescollectionView.centerPoint)
        {
            let value = centerindexpath[1]
            if indexPath.item == value{
                
                print(cell.lbl_Date.text)
                cell.BG_view.backgroundColor = UIColor.systemBlue
            }
            else{
                if #available(iOS 13.0, *) {
                    cell.BG_view.backgroundColor = UIColor.systemBackground
                } else {
                    // Fallback on earlier versions
                }
            }
        }
        // Use the outlet in our custom class to get a reference to the UILabel in the cell
       //   cell.myLabel.text = self.items[indexPath.row] // The row value is the same as the index of the desired text within the array.
      //    cell.backgroundColor = UIColor.cyan // make cell more visible in our example project
          
          return cell
      }
    
     func scrollViewDidScroll(_ scrollView: UIScrollView) {
        datescollectionView.reloadData()
     }

// MARK: - UICollectionViewDelegate protocol
      func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
          // handle tap events
          print("You selected cell #\(indexPath.item)!")
    }
    
// MARK - UITableView Delegates
   func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return  aryNotificationList.count
   }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as? NotificationListCell?
        cell?!.selectionStyle = .none
        return cell as! UITableViewCell
   }

   func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
   }
    
    @IBAction func notification_Action(_ sender: UIButton) {
        let alert = UIAlertController(title: "Make call to 911 ?", message: "Are you sure that you want to call the 911 emergency service?", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: nil))
        alert.addAction(UIAlertAction(title: "Call", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}
class NotificationListCell: UITableViewCell {
}

class datesCell: UICollectionViewCell {
    @IBOutlet weak var lbl_Date: UILabel!
    @IBOutlet weak var BG_view: UIView!
}

extension UICollectionView {
    var centerPoint : CGPoint {
        get {
            return CGPoint(x: self.center.x + self.contentOffset.x, y: self.center.y + self.contentOffset.y);
        }
    }
    var centerCellIndexPath: IndexPath? {
        
        if let centerIndexPath: IndexPath  = self.indexPathForItem(at: self.centerPoint) {
            return centerIndexPath
        }
        return nil
    }
}
