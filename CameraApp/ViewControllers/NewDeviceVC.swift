//
//  NewDeviceVC.swift
//  CameraApp
//
//  Created by macexpert on 07/10/20.
//  Copyright © 2020 macexpert. All rights reserved.
//

import UIKit

class NewDeviceVC: UIViewController,UITableViewDataSource,UITableViewDelegate {

    @IBOutlet var tblView_NewDevices: UITableView!
    @IBOutlet var btn_PofileImg: UIButton!
    
    let aryNotificationList = ["House Entrance", "Kitchen", "House Entrance", "Kitchen"]
    var items = ["1"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.isNavigationBarHidden = true
        
    // For set the corners rounded
        btn_PofileImg.layer.cornerRadius = btn_PofileImg.frame.height / 2
        btn_PofileImg.clipsToBounds = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
           let checkMode =  "\(UserDefaults.standard.value(forKey: "MODETYPE") ?? "")"
            if checkMode == "DARK"{
                if #available(iOS 13.0, *) {
                          overrideUserInterfaceStyle = .dark
                        } else {
                            // Fallback on earlier versions
                        }
            } else {
                if #available(iOS 13.0, *) {
                    overrideUserInterfaceStyle = .light
                } else {
                    // Fallback on earlier versions
                }
            }
}
    
// MARK - UITableView Delegates
   func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return  items.count
   }
    
   func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as? NewDeviceCell?
    cell?!.selectionStyle = .none
    return cell as! UITableViewCell
   }
   func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
   }
    @IBAction func profile_Tapped(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "profileVC") as! profileVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func connect_Action(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "ConnectedDeviceVC") as! ConnectedDeviceVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

class NewDeviceCell: UITableViewCell {
}

