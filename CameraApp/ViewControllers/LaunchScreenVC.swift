//
//  LaunchScreenVC.swift
//  CameraApp
//
//  Created by macexpert on 23/10/20.
//  Copyright © 2020 macexpert. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
class LaunchScreenVC: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
     //   sleep(UInt32(6.0))
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.50) {
            // your code here
            
            if #available(iOS 13.0, *) {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                   self.navigationController?.pushViewController(vc, animated: true)
                   
               } else {
                   // Fallback on earlier versions
             }
        }
    }
}
