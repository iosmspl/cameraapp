//
//  SettingVC.swift
//  CameraApp
//
//  Created by macexpert on 07/10/20.
//  Copyright © 2020 macexpert. All rights reserved.
//

import UIKit
import Foundation
import SystemConfiguration.CaptiveNetwork

class SettingVC: UIViewController {
    
    @IBOutlet var nightModeSwitch: UISwitch!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        let checkMode =  "\(UserDefaults.standard.value(forKey: "MODETYPE") ?? "")"
        if checkMode == "DARK"{
            if #available(iOS 13.0, *) {
                      nightModeSwitch.isOn = true
                      overrideUserInterfaceStyle = .dark
                    } else {
                        // Fallback on earlier versions
                    }
        } else {
            if #available(iOS 13.0, *) {
                nightModeSwitch.isOn = false
                overrideUserInterfaceStyle = .light
            } else {
                // Fallback on earlier versions
            }
        }
        
        if #available(iOS 13.0, *) {
            nightModeSwitch.addTarget(self, action:#selector(SettingVC.switchIsChanged(nightModeSwitch:)), for: UIControl.Event.valueChanged)
        } else {
            // Fallback on earlier versions
        }
    }
    
      @objc func switchIsChanged(nightModeSwitch: UISwitch) {
        
        if nightModeSwitch.isOn {
            print("UISwitch is ON")
            if #available(iOS 13.0, *) {
                      overrideUserInterfaceStyle = .dark
                      UserDefaults.standard.set("DARK", forKey: "MODETYPE")
                
                    } else {
                        // Fallback on earlier versions
                    }
        } else {
            print("UISwitch is OFF")
            if #available(iOS 13.0, *) {
                overrideUserInterfaceStyle = .light
                UserDefaults.standard.set("LIGHT", forKey: "MODETYPE")
            } else {
                // Fallback on earlier versions
            }
        }
}
    @IBAction func back_Action(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func wifiConnections_Action(_ sender: UIButton) {
        let ssid = self.getAllWiFiNameList()
        print("SSID: \(ssid)")
    }
    
    func getAllWiFiNameList() -> String? {
              var ssid: String?
              if let interfaces = CNCopySupportedInterfaces() as NSArray? {
              for interface in interfaces {
              if let interfaceInfo = CNCopyCurrentNetworkInfo(interface as! CFString) as NSDictionary? {
                          ssid = interfaceInfo[kCNNetworkInfoKeySSID as String] as? String
                          break
                      }
                  }
              }
              return ssid
          }
    
}
