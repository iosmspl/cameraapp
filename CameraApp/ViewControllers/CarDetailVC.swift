//
//  CarDetailVC.swift
//  CameraApp
//
//  Created by macexpert on 01/10/20.
//  Copyright © 2020 macexpert. All rights reserved.
//

import UIKit

class CarDetailVC: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet var tblViewActivity : UITableView!
    
    let aryActivities = ["House Entrance", "Kitchen", "House Entrance", "Kitchen"]

    @IBOutlet var top_ImgView : UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let checkMode =  "\(UserDefaults.standard.value(forKey: "MODETYPE") ?? "")"
        if checkMode == "DARK"{
            if #available(iOS 13.0, *) {    
                      overrideUserInterfaceStyle = .dark
                    } else {
                        // Fallback on earlier versions
                    }
        } else {
            if #available(iOS 13.0, *) {
                overrideUserInterfaceStyle = .light
            } else {
                // Fallback on earlier versions
            }
        }
}
    
// MARK - UITableView Delegates
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return  aryActivities.count
        }
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

         let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as? ActivityListCell?
         cell?!.selectionStyle = .none
         return cell as! UITableViewCell
        }
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            
        }

    @IBAction func setting_Tapped(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "SettingVC") as! SettingVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func back_Action(_ sender: UIButton) {
        self.tabBarController?.tabBar.isHidden = false
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func share_Tapped(_ sender: Any) {

        // Implement code wherein you take snapshot of the screen if needed. For illustration purposes, assumed an image stored as asset.
        
        let image: UIImage = UIImage(named: "CarImage")!
        
        let activityVC = UIActivityViewController(activityItems: [image], applicationActivities: nil)
        activityVC.popoverPresentationController?.sourceView = self.view
        self.present(activityVC, animated: true, completion: nil)
        
   /*
        // Share url
        ...
        let activityVC = UIActivityViewController(activityItems: ["www.google.com"], applicationActivities: nil)
        ...
        // Share text
        ...
        let activityVC = UIActivityViewController(activityItems: ["This is nifty swifty"], applicationActivities: nil)
        // Save image in photo library
        ...
        UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
        ...
        */
    }
}

class ActivityListCell: UITableViewCell {
    
}
