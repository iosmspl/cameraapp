//
//  HomeVC.swift
//  CameraApp
//
//  Created by macexpert on 30/09/20.
//  Copyright © 2020 macexpert. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

var  strselectedOption = ""

class HomeVC: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet var tbl_View: UITableView!
    @IBOutlet var gpdBG_view : UIView!

    @IBOutlet var segment_BGView: UIView!
    
 // Buttons outlets
    @IBOutlet weak var btnHouse: UIButton!
    @IBOutlet weak var btnCar: UIButton!
    
    let aryHouses = ["House Entrance", "Kitchen", "House Entrance", "Kitchen"]
    let aryCars = ["Mom's Car", "Dad's Car", "Mom's Car", "Dad's Car"]
    
    var HouseImages: [UIImage] = [
        UIImage(named: "HouseImage")!,
        UIImage(named: "HouseImage")!,
        UIImage(named: "HouseImage")!,
        UIImage(named: "HouseImage")!
    ]
    var CarImages: [UIImage] = [
           UIImage(named: "CarImage")!,
           UIImage(named: "CarImage")!,
           UIImage(named: "CarImage")!,
           UIImage(named: "CarImage")!
       ]
    
// For video Player Fucntionality
    var playerViewController=AVPlayerViewController()
    var playerView = AVPlayer()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        print(segment_BGView.frame.height)
        
// Set the Corner Radius Rounded accoridng to view
        segment_BGView.layer.cornerRadius = segment_BGView.frame.height / 2
        
    //  Intialising Values
        strselectedOption = "House"
        gpdBG_view.isHidden = true
        
     // tableview
        tbl_View.dataSource = self
        tbl_View.delegate = self
    }
    override func viewWillAppear(_ animated: Bool) {
        let checkMode =  "\(UserDefaults.standard.value(forKey: "MODETYPE") ?? "")"
         if checkMode == "DARK"{
            
             if #available(iOS 13.0, *) {
                overrideUserInterfaceStyle = .dark
    //  UITabBar.appearance().barTintColor = UIColor.black // your color
                self.tabBarController?.tabBar.backgroundColor = .black
                self.tabBarController?.tabBar.barTintColor =  UIColor.black
                
             } else {
                         // Fallback on earlier versions
                     }
         } else {
             if #available(iOS 13.0, *) {
                 overrideUserInterfaceStyle = .light
              // UITabBar.appearance().barTintColor = UIColor.white // your color
                 self.tabBarController?.tabBar.backgroundColor = .white
                 self.tabBarController?.tabBar.barTintColor =  UIColor.white

             } else {
                 // Fallback on earlier versions
             }
         }
    }
    // 5
    // MARK - UITableView Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        if strselectedOption == "House"{
            return HouseImages.count
        }
        else{
            return CarImages.count
        }
        return 0
    }
            
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

     let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as? ListCell?
     cell?!.selectionStyle = .none
        
        if strselectedOption == "House"{
              cell?!.img_View.image  = HouseImages[indexPath.row]
              cell?!.lbl_Name.text  = aryHouses[indexPath.row]
             gpdBG_view.isHidden = true
          }
          else{
              cell?!.img_View.image  = CarImages[indexPath.row]
              cell?!.lbl_Name.text  = aryCars[indexPath.row]
              gpdBG_view.isHidden = false
          }
    //  cell?.textLabel?.numberOfLines = 0
        return cell as! UITableViewCell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
       if strselectedOption == "House"{
        
//         let vc = storyboard?.instantiateViewController(withIdentifier: "videoVC") as! videoVC
//         self.navigationController?.pushViewController(vc, animated: true)
        
        let fileURL = NSURL(fileURLWithPath: "/Users/macexpert/Desktop/JOHN_MSPL/ConnectedSolutions/cameraapp/CameraApp/video.mp4")
         playerView = AVPlayer(url: fileURL as URL)
         playerViewController.player = playerView
         self.present(playerViewController,animated: true){
         self.playerViewController.player?.play()
            
        }
    }
       else{
            let vc = storyboard?.instantiateViewController(withIdentifier: "CarDetailVC") as! CarDetailVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func buttonHouse(sender: AnyObject) {
             strselectedOption = "House"
             btnHouse.backgroundColor = UIColor.systemBlue
             btnCar.backgroundColor = UIColor(red: 74.0/255.0, green: 180.0/255.0, blue: 251.0/255.0, alpha: 1)

             btnHouse.setTitleColor(UIColor.white, for: .normal)
             btnHouse.setTitleColor(UIColor.white, for: .selected)
             btnCar.setTitleColor(UIColor(red: 74.0/255.0, green: 180.0/255.0, blue: 251.0/255.0, alpha: 1), for: .normal)
             self.tbl_View.reloadData()
      }

    @IBAction func buttonCar(sender: AnyObject){
        
           strselectedOption = "Car"
           btnCar.backgroundColor = UIColor.systemBlue
           btnHouse.backgroundColor = UIColor(red: 74.0/255.0, green: 180.0/255.0, blue: 251.0/255.0, alpha: 1)
          
           btnCar.setTitleColor(UIColor.white, for: .normal)
           btnCar.setTitleColor(UIColor(red: 74.0/255.0, green: 180.0/255.0, blue: 251.0/255.0, alpha: 1), for: .selected)
           btnHouse.setTitleColor(UIColor(red: 74.0/255.0, green: 180.0/255.0, blue: 251.0/255.0, alpha: 1), for: .selected)

         self.tbl_View.reloadData()
    }
    @IBAction func gpsTapped(sender: AnyObject){
        let vc = storyboard?.instantiateViewController(withIdentifier: "GPSVC") as! GPSVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func add_Action(_ sender: UIButton) {

//           let alert = UIAlertController(title: "Device not found !", message: "We couldn't find a new device near you,please check the internet connections on all your devices", preferredStyle: UIAlertController.Style.alert)
//           alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: nil))
//           alert.addAction(UIAlertAction(title: "Try Again", style: UIAlertAction.Style.default, handler: nil))
//           self.present(alert, animated: true, completion: nil)

            let alertView = UIAlertController(title: "Device not found !", message: "We couldn't find a new device near you,please check the internet connections on all your devices", preferredStyle: .alert)
            alertView.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: nil))
            alertView.addAction(UIAlertAction(title: "Try Again", style: UIAlertAction.Style.default, handler: { (act) in
                print("Ok Button pressed")
                
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "NewDeviceVC") as! NewDeviceVC
                self.navigationController?.pushViewController(vc, animated: true)
                
        }))
            self.present(alertView, animated: true, completion: nil)
    }
}

class ListCell: UITableViewCell {
    @IBOutlet var img_View: UIImageView!
    @IBOutlet var lbl_Name: UILabel!
}
