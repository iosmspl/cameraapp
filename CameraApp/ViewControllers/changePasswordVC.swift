//
//  changePasswordVC.swift
//  CameraApp
//
//  Created by macexpert on 14/10/20.
//  Copyright © 2020 macexpert. All rights reserved.
//

import UIKit

class changePasswordVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let checkMode =  "\(UserDefaults.standard.value(forKey: "MODETYPE") ?? "")"
         if checkMode == "DARK"{
             if #available(iOS 13.0, *) {
                       overrideUserInterfaceStyle = .dark
                     } else {
                         // Fallback on earlier versions
                     }
         } else {
             if #available(iOS 13.0, *) {
                 overrideUserInterfaceStyle = .light
             } else {
                 // Fallback on earlier versions
             }
         }
    }
    
    @IBAction func back_Tapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func save_Tapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
