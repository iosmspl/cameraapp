//
//  AppDelegate.swift
//  CameraApp
//
//  Created by macexpert on 29/09/20.
//  Copyright © 2020 macexpert. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import GoogleSignIn

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    weak var screen : UIView? = nil // property of the AppDelegate

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        GIDSignIn.sharedInstance()?.clientID = "606901667896-d68f02rqn0n7iemc123ufqbja12srbbu.apps.googleusercontent.com"
        
        IQKeyboardManager.shared.enable = true

    // Check the Light/Dark Mode enabled in app setting......
               let checkMode =  "\(UserDefaults.standard.value(forKey: "MODETYPE") ?? "")"
               print("checkMode== \(checkMode)")

               if checkMode == "DARK"{
                if #available(iOS 13.0, *) {
                   window!.overrideUserInterfaceStyle = .dark

                     //  self.inputView?.backgroundColor = UIColor.black
                     //   sleep(UInt32(10.0))
                           } else {
                               // Fallback on earlier versions
                           }
               } else {
                   if #available(iOS 13.0, *) {
                       window!.overrideUserInterfaceStyle = .light
                   } else {
                       // Fallback on earlier versions
                   }
           }
                return true
    }
    
   
    func applicationWillResignActive(_ application: UIApplication) {
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        window!.rootViewController?.dismiss(animated: false)
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    //Sign in with Google
       func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
           return (GIDSignIn.sharedInstance()?.handle(url))!
       }
}
