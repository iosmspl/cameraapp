//
//  GPSVC.swift
//  CameraApp
//
//  Created by macexpert on 01/10/20.
//  Copyright © 2020 macexpert. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class GPSVC: UIViewController {
    
    @IBOutlet var mapView: MKMapView!
    
    fileprivate let locationManager:CLLocationManager = CLLocationManager()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        locationManager.delegate = self
        checkLocationAuthorizationStatus()
    }
    override func viewWillAppear(_ animated: Bool) {
        let checkMode =  "\(UserDefaults.standard.value(forKey: "MODETYPE") ?? "")"
         if checkMode == "DARK"{
             if #available(iOS 13.0, *) {
                       overrideUserInterfaceStyle = .dark
                     } else {
                         // Fallback on earlier versions
                     }
         } else {
             if #available(iOS 13.0, *) {
                 overrideUserInterfaceStyle = .light
             } else {
                 // Fallback on earlier versions
             }
         }
    }
    
    func checkLocationAuthorizationStatus() {
        
      let status = CLLocationManager.authorizationStatus()
        if status == CLAuthorizationStatus.notDetermined{
             print("NotDetermined")
             locationManager.requestWhenInUseAuthorization()
             CLLocationManager.locationServicesEnabled()
             locationManager.requestLocation()

         }else {
             print("Problem with authorization")

         }
    }
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print("got location update")
    }
    func locationManager(manager: CLLocationManager,  didFailWithError error: NSError) {
        print("got following error from loaction manager")
        print(error.description)
    }

    func locationManager(manager: CLLocationManager,
        didChangeAuthorizationStatus status: CLAuthorizationStatus){
            print("didChangeAuthorizationStatus")
            checkLocationAuthorizationStatus()
    }
    @IBAction func back_Action(_ sender: UIButton) {
           self.navigationController?.popViewController(animated: true)
       }
}
extension GPSVC : CLLocationManagerDelegate {

    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
         print("error:: \(error.localizedDescription)")
    }

    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            locationManager.requestLocation()
        }
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {

        if locations.first != nil {
            print("location:: (location)")
        }

    }

}
