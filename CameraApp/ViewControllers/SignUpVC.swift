//
//  SignUpVC.swift
//  CameraApp
//
//  Created by macexpert on 29/09/20.
//  Copyright © 2020 macexpert. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
class SignUpVC: UIViewController {
    
    @IBOutlet var name_BG_View: UIView!
    @IBOutlet var email_BG_View: UIView!
    @IBOutlet var pwd_BG_View: UIView!
    @IBOutlet var pinCode_BG_View: UIView!
    
// UITextFields
    @IBOutlet var txt_Name: UITextField!
    @IBOutlet var txt_Email: UITextField!
    @IBOutlet var txt_Password: UITextField!
    @IBOutlet var txt_PinCode : UITextField!

// UIButtons
    @IBOutlet var btn_SignUp: UIButton!
    
    override func viewDidLoad() {

           super.viewDidLoad()
           self.navigationController?.isNavigationBarHidden = true
        
        // Name Background View
           self.name_BG_View.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
           self.name_BG_View.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
           self.name_BG_View.layer.shadowOpacity = 1.0
           self.name_BG_View.layer.shadowRadius = 0.0
           self.name_BG_View.layer.masksToBounds = false
           self.name_BG_View.layer.cornerRadius = 5.0

        // Email ID Background View
           self.email_BG_View.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
           self.email_BG_View.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
           self.email_BG_View.layer.shadowOpacity = 1.0
           self.email_BG_View.layer.shadowRadius = 0.0
           self.email_BG_View.layer.masksToBounds = false
           self.email_BG_View.layer.cornerRadius = 5.0

        // Password Background View
           self.pwd_BG_View.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
           self.pwd_BG_View.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
           self.pwd_BG_View.layer.shadowOpacity = 1.0
           self.pwd_BG_View.layer.shadowRadius = 0.0
           self.pwd_BG_View.layer.masksToBounds = false
           self.pwd_BG_View.layer.cornerRadius = 5.0
        
        // PinCode Background View
           self.pinCode_BG_View.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
           self.pinCode_BG_View.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
           self.pinCode_BG_View.layer.shadowOpacity = 1.0
           self.pinCode_BG_View.layer.shadowRadius = 0.0
           self.pinCode_BG_View.layer.masksToBounds = false
           self.pinCode_BG_View.layer.cornerRadius = 5.0
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let checkMode =  "\(UserDefaults.standard.value(forKey: "MODETYPE") ?? "")"
         if checkMode == "DARK"{
             if #available(iOS 13.0, *) {
                       overrideUserInterfaceStyle = .dark
                     } else {
                         // Fallback on earlier versions
                     }
         } else {
             if #available(iOS 13.0, *) {
                 overrideUserInterfaceStyle = .light
             } else {
                 // Fallback on earlier versions
             }
         }
    }
    
    @IBAction func login_Tapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func Signup_Tapped(_ sender: UIButton) {
        
        if (txt_Name.text == "")
        {
            let message = UIAlertView(title: "ConnectedSolutions", message: "Please enter Name", delegate: nil, cancelButtonTitle: "OK")
            message.show()
         }
        else if (txt_Email.text == "")
        {
            let message = UIAlertView(title: "ConnectedSolutions", message: "Please enter Email ID", delegate: nil, cancelButtonTitle: "OK")
            message.show()
        }
        else if !(txt_Email.text?.isvaliddemail())!{
           let message = UIAlertView(title: "ConnectedSolutions", message: "Please enter Valid Email", delegate: nil, cancelButtonTitle: "OK")
           message.show()
         }
         else if (txt_Password.text == "")
         {
            let message = UIAlertView(title: "ConnectedSolutions", message: "Please enter your password", delegate: nil, cancelButtonTitle: "OK")
             message.show()
         }
         else if (txt_PinCode.text == "")
           {
              let message = UIAlertView(title: "ConnectedSolutions", message: "Please enter your PinCode", delegate: nil, cancelButtonTitle: "OK")
               message.show()
         }
         else if (txt_Name.text != ""  && txt_Email.text != "" && txt_Password.text != "" && txt_PinCode.text != "")
         {
            if let image = UIImage(named: "button-login (active)-1") {
               btn_SignUp.setImage(image, for: .normal)

                let vc = storyboard?.instantiateViewController(withIdentifier: "PinCodeVC") as! PinCodeVC
                self.navigationController?.pushViewController(vc, animated: true)
            }
         }
        else {
         //   loginApi()
            let vc = storyboard?.instantiateViewController(withIdentifier: "PinCodeVC") as! PinCodeVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}
