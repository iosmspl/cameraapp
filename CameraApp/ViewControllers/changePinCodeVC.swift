//
//  changePinCodeVC.swift
//  CameraApp
//
//  Created by macexpert on 14/10/20.
//  Copyright © 2020 macexpert. All rights reserved.
//

import UIKit
import OTPFieldView

class changePinCodeVC: UIViewController {
    
    @IBOutlet var otpTextFieldView: OTPFieldView!
    @IBOutlet var top_BGView: UIView!
    
    override func viewDidLoad() {
        
       super.viewDidLoad()
          
       self.top_BGView.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha:  0.25).cgColor
       self.top_BGView.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
       self.top_BGView.layer.shadowOpacity = 1.0
       self.top_BGView.layer.shadowRadius = 0.0
       self.top_BGView.layer.masksToBounds = false
        
       setupOtpView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let checkMode =  "\(UserDefaults.standard.value(forKey: "MODETYPE") ?? "")"
         if checkMode == "DARK"{
             if #available(iOS 13.0, *) {
                       overrideUserInterfaceStyle = .dark
                     } else {
                         // Fallback on earlier versions
                     }
         } else {
             if #available(iOS 13.0, *) {
                 overrideUserInterfaceStyle = .light
             } else {
                 // Fallback on earlier versions
             }
         }
    }

    func setupOtpView(){
           self.otpTextFieldView.fieldsCount = 4
           self.otpTextFieldView.fieldBorderWidth = 2
           self.otpTextFieldView.defaultBorderColor = UIColor.gray
           self.otpTextFieldView.filledBorderColor = UIColor.black
           self.otpTextFieldView.cursorColor = UIColor.red
           self.otpTextFieldView.displayType = .underlinedBottom
           self.otpTextFieldView.fieldSize = 40
           self.otpTextFieldView.separatorSpace = 8
           self.otpTextFieldView.shouldAllowIntermediateEditing = false
           self.otpTextFieldView.delegate = self
           self.otpTextFieldView.initializeUI()
      }
    
    @IBAction func back_Tapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
       }
}

extension changePinCodeVC: OTPFieldViewDelegate {
    func hasEnteredAllOTP(hasEnteredAll hasEntered: Bool) -> Bool {
        print("Has entered all OTP? \(hasEntered)")
        return false
    }
    func shouldBecomeFirstResponderForOTP(otpTextFieldIndex index: Int) -> Bool {
        return true
    }
    func enteredOTP(otp otpString: String) {
        print("OTPString: \(otpString)")
    }
}
